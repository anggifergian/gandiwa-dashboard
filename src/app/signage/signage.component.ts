import { Component, OnInit } from '@angular/core';
import { AuthenticationLoopbackService } from '../services/authentication-loopback.service';
import { Router } from '@angular/router';
import { SharedService } from '../services/shared.service';

@Component({
    selector: 'app-signage-component',
    templateUrl: './signage.component.html',
    styleUrls: ['./signage.component.css']
})
export class SignageComponent implements OnInit {

    userLevel;
    userCookie = {
        signature: '',
        username: '',
        userlevel: ''
    };

    constructor(
        private authService: AuthenticationLoopbackService,
        private router: Router,
        private sharedService: SharedService
    ) {}

    ngOnInit() {
        const module = this.authService.getUserLevel();
        if (module === '0') {
            this.userLevel = 'client';
        } else {
            this.userLevel = 'admin';
        }
    }

    logout() {
        this.authService.clearCookieData();
        const message = {
            status: 'logout',
            detail: 'Anda telah logout, silakan login kembali'
        };
        this.sharedService.set_object(message);
        this.router.navigateByUrl('/login');
    }

    goToDashboard() {
        this.router.navigate(['/signage/dashboard']);
    }
}
