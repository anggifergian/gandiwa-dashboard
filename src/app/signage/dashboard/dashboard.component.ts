import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ChangeDetectorRef,
} from '@angular/core';
import { DashboardService } from './dashboard.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
    private ngUnsubscribe = new Subject();

    @Input() date: Date;
    @Output() dateChange = new EventEmitter<Date>();
    cookieData;
    selectedOption = { time: null };
    selectedMonth = { month: null, value: null };
    selectedDate = new Date();
    devicelist;
    formatedDate;
    selectedLocation;
    selectedLocationName;
    locationStatistics;

    isLocationLoading = false;
    isImpressionLoading = false;
    isDwellTimeLoading = false;
    isPlatformLoading = false;
    isSegmenLoading = false;
    isLocations =  false;
    isImpression = false;
    isDwellTime = false;
    isAvgDwellTime = false;
    isPlatform = false;
    isSegmen = false;

    resultOptions = [
        { time: 'Daily' },
        { time: 'Monthly' }, ];
    monthList = [
        { month: 'Januari', value: 1 },
        { month: 'Februari', value: 2 },
        { month: 'Maret', value: 3 },
        { month: 'April', value: 4 },
        { month: 'Mei', value: 5 },
        { month: 'Juni', value: 6 },
        { month: 'Juli', value: 7 },
        { month: 'Agustus', value: 8 },
        { month: 'September', value: 9 },
        { month: 'Oktober', value: 10 },
        { month: 'November', value: 11 },
        { month: 'Desember', value: 12 }, ];

    chartLabel;

    chartOption = {
        scales: {
            xAxes: [{
                scaleLabel: { display: true, labelString: '' },
                ticks: { display: true, major: { fontStyle: 'bold', fontColor: '#FF0000' }},
                gridLines: { display: false }
            }],
            yAxes: [{
                ticks: { beginAtZero: false },
                gridLines: { display: false }
            }]
        }};

    @ViewChild('impressionChart') impressionChart: any;
    @ViewChild('dwellTimeChart') dwellTimeChart: any;
    @ViewChild('platformChart') platformChart: any;
    @ViewChild('segmentChart') segmentChart: any;
    impressionChartData = {
        labels: null,
        datasets: null };
    dwellTimeChartData = {
        labels: null,
        datasets: null };
    platformChartData = {
        labels: null,
        datasets: null };
    segmentChartData = {
        labels: null,
        datasets: null };

    impressionTotal = 0;
    reachTotal = 0;
    playedTotal = 0;
    dwellAvg = 0;

    constructor(
        private dashboardService: DashboardService,
        private cdRef: ChangeDetectorRef,
        private cookieService: CookieService
    ) {}

    ngOnInit() {
        this.dashboardService.getLocations()
            .pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            data => this.devicelist = data);

        // Set initial option
        this.selectedOption.time = this.resultOptions[0].time;

        // Set initial day value
        this.formatedDate = moment().subtract(1, 'days').format('YYYY-MM-DD');

        // Set initial month value
        const currentMonth = moment().month();
        this.getCurrentMonth(currentMonth);

        // Get cookie data
        const cookieUser = { signature: '', userlevel: '', username: '' };
        this.cookieData = Object.assign(cookieUser, this.cookieService.getAll());

        // Convert to yesterday
        const today = new Date();
        this.selectedDate.setDate(today.getDate() - 1);
    }
    getCurrentMonth(initial) {
        const initialMonth = { month: null, value: null };
        this.selectedMonth = Object.assign(initialMonth,
            this.monthList.find(month => month.value === initial + 1));
    }
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    selectLocation(event) {
        this.selectedLocation = event.value.devicename;
        this.selectedLocationName = event.value.devicealias;
    }
    selectResultOption(event) {
        this.selectedOption.time = event.value.time;
        this.chartLabel = this.selectedOption.time === 'Daily' ? 'Jam' : 'Tanggal';
        // console.log(this.chartOption.scales.xAxes, this.chartLabel);
    }

    onSelectMonth(event) {
        this.selectedMonth = event.value;
    }
    onSelectDate(): void {
        this.dateChange.emit(this.selectedDate);
        this.formatedDate = moment(this.selectedDate).format('YYYY-MM-DD');
    }
    showResult(): void {
        if (this.selectedLocation && this.formatedDate || this.selectedLocation && this.selectedMonth.value ) {
            const params = {
                devicename: this.selectedLocation,
                filterlocalmac: 0,
                signature: this.cookieData.signature,
                username: this.cookieData.username
            };
            if (this.selectedOption.time === 'Daily') {
                const dailyParams = Object.assign(params, {
                    tanggal: this.formatedDate,
                });
                this.dailyLocations(dailyParams);
                this.dailyImpression(dailyParams);
                this.dailyDwellTime(dailyParams);
                this.dailyAvgDwellTime(dailyParams);
                this.dailyPlatform(dailyParams);
                this.dailySegmentation(dailyParams);
                this.dailyReach(dailyParams);
            } else {
                const monthlyParams = Object.assign(params, {
                    bulan: this.selectedMonth.value,
                    tahun: 2020
                });
                this.monthlyLocations(monthlyParams);
                this.monthlyImpression(monthlyParams);
                this.monthlyDwellTime(monthlyParams);
                this.monthlyAvgDwellTime(monthlyParams);
                this.monthlyPlatform(monthlyParams);
                this.monthlySegmentation(monthlyParams);
                this.monthlyReach(monthlyParams);
            }
        } else {
            alert('Pilih lokasi sama tanggal dulu cuy');
        }
    }

    searchGeneral(forms) {
        const value = forms.value;

        const params = {
            devicename: this.selectedLocation,
            filterlocalmac: 0,
            signature: this.cookieData.signature,
            username: this.cookieData.username
        };

        if (this.selectedOption.time === 'Daily') {
            const dailyParams = Object.assign(params, {
                tanggal: this.formatedDate,
            });
            this.dailyLocations(dailyParams);
            this.dailyImpression(dailyParams);
            this.dailyDwellTime(dailyParams);
            this.dailyAvgDwellTime(dailyParams);
            this.dailyPlatform(dailyParams);
            this.dailySegmentation(dailyParams);
            this.dailyReach(dailyParams);
        } else {
            const monthlyParams = Object.assign(params, {
                bulan: this.selectedMonth.value,
                tahun: 2020
            });
            this.monthlyLocations(monthlyParams);
            this.monthlyImpression(monthlyParams);
            this.monthlyDwellTime(monthlyParams);
            this.monthlyAvgDwellTime(monthlyParams);
            this.monthlyPlatform(monthlyParams);
            this.monthlySegmentation(monthlyParams);
            this.monthlyReach(monthlyParams);
        }
    }

    refreshChart(type) {
        setTimeout(() => {
            if (type === 'impression') {
                this.impressionChart.refresh();
            } else if (type === 'dwelltime') {
                this.dwellTimeChart.refresh();
            } else if (type === 'platform') {
                this.platformChart.refresh();
            } else if (type === 'segment') {
                this.segmentChart.refresh();
            }
        }, 100);
    }
    showImpressionChart(data, type) {
        const dataImpression = data;
        const datasets = [];
        const labels = [];
        const dataset = {
            label: type === 'Daily' ? 'Hourly Impression' : 'Monthly Impression',
            data: [],
            backgroundColor: ['rgba(55, 202, 141, 0.61)'],
            borderWidth: 1
        };

        dataImpression.map(imp => {
            labels.push(type === 'Daily' ? imp.jam : imp.tanggal);
            dataset.data.push(+imp.jumlah);
        });
        // console.log(dataset.data);
        this.impressionTotal = dataset.data.reduce((a, b) => a + b, 0);
        datasets.push(dataset);
        this.impressionChartData.labels = labels;
        this.impressionChartData.datasets = datasets;
        this.refreshChart('impression');
        this.isImpressionLoading = false;
    }
    showDwellTimeChart(data, type) {
        const dataImpression = data;
        const datasets = [];
        const labels = [];
        const dataset = {
            label: type === 'Daily' ? 'Hourly Dwell Time' : 'Monthly Dwell Time',
            data: [],
            backgroundColor: ['rgba(55, 202, 141, 0.61)'],
            borderWidth: 1
        };

        dataImpression.map(imp => {
            labels.push(type === 'Daily' ? imp.jam : imp.tanggal);
            dataset.data.push(+imp.dwelltime);
        });
        datasets.push(dataset);
        this.dwellTimeChartData.labels = labels;
        this.dwellTimeChartData.datasets = datasets;
        this.refreshChart('dwelltime');
        this.isDwellTimeLoading = false;
    }
    showPlatformChart(data) {
        const dataPlatform = data;
        const datasets = [];
        const labels = [];
        const newDataset = {
            data: [],
            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']};

        dataPlatform.map(plt => {
            labels.push(plt.platform);
            newDataset.data.push(+plt.count);
        });
        datasets.push(newDataset);
        this.platformChartData.labels = labels;
        this.platformChartData.datasets = datasets;
        this.refreshChart('platform');
        this.isPlatformLoading = false;
    }
    showSegmentChart(data) {
        const dataSegment = data;
        const datasets = [];
        const labels = [];
        const newDataSet = {
            data: [],
            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']};

        dataSegment.map(smt => {
            labels.push(smt.segment);
            newDataSet.data.push(+smt.count);
        });
        datasets.push(newDataSet);
        const newLabels = labels.slice(0, 3);
        this.segmentChartData.labels = newLabels;
        this.segmentChartData.datasets = datasets;
        this.refreshChart('segment');
        this.isSegmenLoading = false;
    }
    showTotalImpression(data) {
        const impression = data;
        const impressionJumlah = [];
        impression.map( res => impressionJumlah.push(+res.jumlah));
        this.impressionTotal = impressionJumlah.reduce((a, b) => a + b, 0);
    }
    showTotalPlayed(data) {
        const played = data;
        const playedJumlah = [];
        played.map( res => playedJumlah.push(+res.played));
        this.playedTotal = playedJumlah.reduce((a, b) => a + b, 0);
    }

    dailyLocations(data) {
        this.isLocations = true;
        this.isLocationLoading = true;
        this.dashboardService.getDailyLocations(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => {
                this.locationStatistics = stats;
                console.log('Dashboard data: ', this.locationStatistics);
                this.showTotalPlayed(stats);
                this.isLocationLoading = false;
            }); }
    monthlyLocations(data) {
        this.isLocations = true;
        this.isLocationLoading = true;
        this.dashboardService.getMonthlyLocations(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => {
                this.locationStatistics = stats;
                this.showTotalPlayed(stats);
                this.isLocationLoading = false;
            }); }
    dailyImpression(data) {
        this.isImpression = true;
        this.isImpressionLoading = true;
        this.dashboardService.getDailyTraffic(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showImpressionChart(stats, 'Daily')); }
    monthlyImpression(data) {
        this.isImpression = true;
        this.isImpressionLoading = true;
        this.dashboardService.getMonthlyTraffic(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showImpressionChart(stats, 'Monthly')); }
    dailyDwellTime(data) {
        this.isDwellTime = true;
        this.isDwellTimeLoading = true;
        this.dashboardService.getDailyDwellTime(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showDwellTimeChart(stats, 'Daily')); }
    monthlyDwellTime(data) {
        this.isDwellTime = true;
        this.isDwellTimeLoading = true;
        this.dashboardService.getMonthlyDwellTime(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showDwellTimeChart(stats, 'Monthly')); }
    dailyAvgDwellTime(data) {
        this.isAvgDwellTime = true;
        this.dashboardService.getDailyAvgDwellTime(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.dwellAvg = +stats[0].Mdwelltime); }
    monthlyAvgDwellTime(data) {
        this.isAvgDwellTime = true;
        this.dashboardService.getMonthlyAvgDwellTime(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.dwellAvg = +stats[0].Mdwelltime); }
    dailyPlatform(data) {
        this.isPlatform = true;
        this.isPlatformLoading = true;
        this.dashboardService.getDailyPlatform(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showPlatformChart(stats)); }
    monthlyPlatform(data) {
        this.isPlatform = true;
        this.isPlatformLoading = true;
        this.dashboardService.getMonthlyPlatform(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.showPlatformChart(stats)); }
    dailySegmentation(data) {
        this.isSegmen = true;
        this.isSegmenLoading = true;
        this.dashboardService.getDailySegmentation(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => {
                this.showSegmentChart(stats);
            }, err => {
                console.log(err);
            }); }
    monthlySegmentation(data) {
        this.isSegmen = true;
        this.isSegmenLoading = true;
        this.dashboardService.getMonthlySegmentation(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => {
                this.showSegmentChart(stats);
            }, err => {
                console.log(err);
            }); }
    dailyBrand(data) {
        this.dashboardService.getDailyBrand(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => console.log('Daily brand: ', stats)); }
    monthlyBrand(data) {
        this.dashboardService.getMonthlyBrand(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => console.log('Monthly brand: ', stats)); }
    dailyReach(data) {
        this.dashboardService.getDailyReach(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.reachTotal = +stats[0].totaltraffic); }
    monthlyReach(data) {
        this.dashboardService.getMonthlyReach(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            stats => this.reachTotal = +stats[0].totaltraffic); }
}
