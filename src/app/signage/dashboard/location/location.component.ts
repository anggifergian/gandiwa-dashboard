import {Component, Input, OnChanges} from '@angular/core';

@Component({
    selector: 'app-location',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnChanges {
    @Input() locations: [];

    constructor() {}

    ngOnChanges() {
        this.locations.map(location => {
            console.log(location);
        });
    }
}
