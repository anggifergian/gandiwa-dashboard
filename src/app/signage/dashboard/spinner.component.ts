import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {
    @HostBinding('style.position') get position() {
        return 'absolute';
    }
    @HostBinding('style.top') get top() {
        return '50%';
    }
    @HostBinding('style.left') get left() {
        return '50%';
    }
    @HostBinding('style.width') get width() {
        return '100%';
    }
    @HostBinding('style.height') get height() {
        return '100%';
    }
    @HostBinding('style.transform') get transform() {
        return 'translate(-50%, -50%)';
    }
    @HostBinding('style.background') get background() {
        return 'rgba(243, 250, 253, 0.8)';
    }
    @HostBinding('style.z-index') get zindex() {
        return '900';
    }
}
