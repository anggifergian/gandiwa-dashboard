import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationLoopbackService } from 'src/app/services/authentication-loopback.service';

@Injectable()
export class DashboardService {
    private apiUrl = environment.apiSignage;
    userCookie = {
        signature: '',
        username: '',
        userlevel: ''
    };

    constructor(
        private http: HttpClient,
        private authService: AuthenticationLoopbackService
    ) {}

    getLocations(): Observable<any> {
        const user = Object.assign(this.userCookie, this.authService.getUserFull());
        return this.http.get(`${this.apiUrl}ajax_pxclist.pl?username=${user.username}&signature=${user.signature}`);
    }
    getDailyLocations(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loadcounter_daily.pl`, { params: data });
    }
    getMonthlyLocations(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loadcounter_monthly.pl`, { params: data });
    }
    getDailyTraffic(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loadtraffic_daily.pl`, { params: data });
    }
    getMonthlyTraffic(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loadtraffic_monthly.pl`, { params: data });
    }
    getDailyDwellTime(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loaddwelltime_daily.pl`, { params: data });
    }
    getMonthlyDwellTime(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_loaddwelltime_monthly.pl`, { params: data });
    }
    getDailyAvgDwellTime(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_currentdwelltime_daily.pl`, { params: data });
    }
    getMonthlyAvgDwellTime(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_currentdwelltime_monthly.pl`, { params: data });
    }
    getDailySegmentation(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_segment_daily.pl`, { params: data });
    }
    getMonthlySegmentation(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_segment_monthly.pl`, { params: data });
    }
    getDailyPlatform(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_platform_daily.pl`, { params: data });
    }
    getMonthlyPlatform(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_platform_monthly.pl`, { params: data });
    }
    getDailyBrand(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_brand_daily.pl`, { params: data });
    }
    getMonthlyBrand(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_brand_monthly.pl`, { params: data });
    }
    getDailyReach(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_totaltraffic_daily.pl`, { params: data });
    }
    getMonthlyReach(data): Observable<any> {
        return this.http.get(`${this.apiUrl}ajax_totaltraffic_monthly.pl`, { params: data });
    }
}
