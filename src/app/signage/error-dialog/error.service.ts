import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from './error-dialog.component';

@Injectable()
export class ErrorService {
    public isDialogOpen = false;
    constructor(
        public dialog: MatDialog
    ) {}

    openDialog(mess: any) {
        if (this.isDialogOpen) {
            return false;
        }
        this.isDialogOpen = true;
        const dialogRef = this.dialog.open(ErrorDialogComponent, {
            width: '300px',
            data: mess
        });

        dialogRef.afterClosed().subscribe(ress => {
            console.log('The dialog was closed');
            this.isDialogOpen = false;
        });
    }
}
