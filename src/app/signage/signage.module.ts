import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { signageRouterConfig } from './signage.routes';
import { AuthGuard } from '../services/auth-guard.service';

@NgModule({
    imports: [
        RouterModule.forChild(signageRouterConfig),
    ],
    providers: [
        AuthGuard,
    ]
})
export class SignageModule {}
