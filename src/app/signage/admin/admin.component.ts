import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from './admin.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent {
    constructor() {}
}
