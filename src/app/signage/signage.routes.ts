import { Routes } from '@angular/router';
import { SignageComponent } from './signage.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../services/auth-guard.service';
import { StatisticComponent } from './statistic/statistic.component';
import { BannerComponent } from './banner/banner.component';
import { AdminComponent } from './admin/admin.component';
import { VisitorComponent } from './visitor/visitor.component';
import { VisitorViewComponent } from './visitor/visitor-view.component';
import { NavbarComponent } from '../components/navbar/navbar.component';

export const signageRouterConfig: Routes = [
    {
        path: '',
        component: SignageComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'admin', component:  AdminComponent },
            { path: 'dashboard', component:  DashboardComponent },
            { path: 'statistic', component:  StatisticComponent },
            { path: 'banner', component: BannerComponent },
            { path: 'visitor', component: VisitorComponent },
            { path: 'visitor-view', component: VisitorViewComponent },
            { path: 'test', component: NavbarComponent },
            { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    }
];
