import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class BannerService {
    apiUrl = environment.apiMain;
    constructor(
        private http: HttpClient
    ) { }

    getBannerList(data): Observable<any> {
        return this.http.get(`${this.apiUrl}banner/ajax_imagelist.pl`, { params: data });
    }
    getBannerImpression(data): Observable<any> {
        return this.http.get(`${this.apiUrl}banner/ajax_impression.pl`, { params: data });
    }
    getBannerImpressionReference(data): Observable<any> {
        return this.http.get(`${this.apiUrl}banner/ajax_impression_ref.pl`, { params: data });
    }
    getBannerClick(data): Observable<any> {
        return this.http.get(`${this.apiUrl}banner/ajax_click.pl`, { params: data });
    }
    getBannerClickReference(data): Observable<any> {
        return this.http.get(`${this.apiUrl}banner/ajax_ click_ref.pl`, { params: data });
    }
}
