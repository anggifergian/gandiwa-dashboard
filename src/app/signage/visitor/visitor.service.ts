import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class VisitorService {
    private apiUrl = environment.apiMain;
    constructor(
        private http: HttpClient
    ) { }

    getVisitorList(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_visitorlist.pl`, { params: data });
    }
    getVisitorData(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_visitordata.pl`, { params: data });
    }
    getVisitorDevice(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_visitordevice.pl`, { params: data });
    }
    getDeviceLoginCounter(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_logintimes.pl`, { params: data });
    }
    getDeviceLoginLog(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_loginlog.pl`, { params: data });
    }
    getVisitorDwellTimeSummary(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_dwelltime_summary.pl`, { params: data });
    }
    getVisitorDwellTimeDetail(data): Observable<any> {
        return this.http.get(`${this.apiUrl}visitor/ajax_dwelltime.pl`, { params: data });
    }
}
