import { Component, OnInit, OnDestroy, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { VisitorService } from './visitor.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
    selector: 'app-visitor-view',
    templateUrl: './visitor-view.component.html',
    styleUrls: ['./visitor-view.component.css']
})
export class VisitorViewComponent implements OnInit, OnDestroy {
    private ngUnsubscribe = new Subject();
    cookieData;
    visitor;
    visitorData;
    visitorDevice;

    constructor(
        private route: ActivatedRoute,
        private visitorService: VisitorService,
        private cookieService: CookieService,
    ) {}

    ngOnInit() {
        // Get cookie data
        const cookieUser = { signature: '', userlevel: '', username: '' };
        this.cookieData = Object.assign(cookieUser, this.cookieService.getAll());

        this.route.queryParams.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => {
                if (ress.visitor) {
                    this.visitor = JSON.parse(ress.visitor);
                    const params = {
                        email: this.visitor.email,
                        signature: this.cookieData.signature,
                        username: this.cookieData.username
                    };
                    this.getVisitorData(params);
                    this.getVisitorDevice(params);

                }
            }
        );
    }
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    getVisitorData(data) {
        this.visitorService.getVisitorData(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => {
                this.visitorData = ress;
                console.log(this.visitorData);
            });
    }
    getVisitorDevice(data) {
        this.visitorService.getVisitorDevice(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => {
                this.visitorDevice = ress;
                console.log(this.visitorDevice);
            });
    }
    getDeviceLoginCounter(data) {
        this.visitorService.getDeviceLoginCounter(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => { console.log('Device login counter: ', ress); }
        );
    }
    getDeviceLoginLog(data) {
        this.visitorService.getDeviceLoginLog(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => { console.log('Device login log: ', ress); }
        );
    }
    getDwellTimeSummary(data) {
        this.visitorService.getVisitorDwellTimeSummary(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => { console.log('Dwelltime summary: ', ress); }
        );
    }
    getDwellTimeDetail(data) {
        this.visitorService.getVisitorDwellTimeDetail(data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            ress => { console.log('Dwelltime detail: ', ress); }
        );
    }
}
