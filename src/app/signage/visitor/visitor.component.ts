import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { VisitorService } from './visitor.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
    selector: 'app-visitor',
    templateUrl: './visitor.component.html',
    styleUrls: ['./visitor.component.css']
})
export class VisitorComponent implements OnInit, OnDestroy {

    private ngUnsubscribe = new Subject();
    visitorAll;
    cookieData;

    constructor(
        private visitorService: VisitorService,
        private cookieService: CookieService,
        private router: Router
    ) { }

    ngOnInit() {
        const cookieUser = { signature: '', userlevel: '', username: '' };
        this.cookieData = Object.assign(cookieUser, this.cookieService.getAll());
        const params = {
            signature: this.cookieData.signature,
            username: this.cookieData.username
        };
        this.visitorService.getVisitorList(params).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            visitor => {
                this.visitorAll = visitor;
            }
        );
    }
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    viewUser(user) {
        this.router.navigate(['/signage/visitor-view'], { queryParams: { visitor: JSON.stringify(user) }});
    }
}
