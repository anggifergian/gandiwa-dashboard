import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LoginService {

    private apiLogin = environment.apiLoginGandiwa;
    constructor(
        private http: HttpClient
    ) {}

    public login(data): Observable<any> {
        const body = new HttpParams().set('username', data.username).set('password', data.password);
        const httpPostOptions = { headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'}) };
        return this.http.post(`${this.apiLogin}ajax_login.pl`, body, httpPostOptions );
    }
}
