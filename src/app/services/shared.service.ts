import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
    private object;
    constructor() {}

    set_object(object) {
        this.object = object;
    }
    retrieve_object() {
        return this.object;
    }
}
