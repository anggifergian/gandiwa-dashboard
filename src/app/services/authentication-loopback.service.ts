import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class AuthenticationLoopbackService {
    private gandiwaUrl = environment.apiLoginGandiwa;

    constructor(
        public http: HttpClient,
        public router: Router,
        public cookieService: CookieService
    ) { }

    postLogin(data): Observable<any> {
        const body = new HttpParams().set('username', data.username).set('password', data.password);
        const config = { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') };
        return this.http.post(`${this.gandiwaUrl}ajax_login.pl`, body, config);
    }
    setCookieUserData(data) {
        if (data.signature) {
            this.cookieService.put('signature', data.signature);
        }
        if (data.username) {
            this.cookieService.put('username', data.username);
        }
        if (data.userlevel) {
            this.cookieService.put('userlevel', data.userlevel);
        }
    }
    getUserFull() {
        return this.cookieService.getAll();
    }
    getUserLevel() {
        return this.cookieService.get('userlevel');
    }
    clearCookieData() {
        this.cookieService.removeAll();
    }
    check() {
        if (this.cookieService.get('signature')) {
            return true;
        }
        return false;
    }
}
