import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthenticationLoopbackService } from './authentication-loopback.service';
import { map, catchError } from 'rxjs/operators';
import { ErrorService } from '../signage/error-dialog/error.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    userCookie = {
        signature: '',
        username: '',
        userlevel: ''
    };

    constructor(
        public authService: AuthenticationLoopbackService,
        public errorDialog: ErrorService
    ) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = Object.assign(this.userCookie, this.authService.getUserFull());
        const userlevel = this.authService.getUserLevel();

        if (userlevel) {
            // tslint:disable-next-line: max-line-length
            req = req.clone({ headers: req.headers.set('Cookie', `username=${token.username}; userlevel=${token.userlevel}; signature=${token.signature}`)});
        }

        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('Berhasil');
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                const data = {
                    reason: error && error.error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                this.errorDialog.openDialog(data);
                return throwError(error);
            }));
    }
}
