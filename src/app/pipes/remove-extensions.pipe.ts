import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'removeExtension' })
export class RemoveExtensionsPipe implements PipeTransform {
    transform(content: string) {
        return content.substring(0, content.lastIndexOf('.')) || content;
    }
}
