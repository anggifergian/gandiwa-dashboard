import {Component, Input} from '@angular/core'

@Component({
    selector: 'app-navbar-link-component',
    templateUrl: './navbar-link.component.html',
    styleUrls: ['./navbar-link.component.css']
})
export class NavbarLinkComponent {
    @Input() icon: string;
    @Input() label: string;
}