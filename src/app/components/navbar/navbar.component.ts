import { Component } from "@angular/core";

@Component({
    selector: 'app-navbar-component',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {
    navbarLists = [
        { icon: 'dashboard', label: 'Dashboard' },
        { icon: 'account', label: 'Account' },
        { icon: 'logout', label: 'Logout' },
    ]
}