import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from '../services/login.service';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationLoopbackService } from '../services/authentication-loopback.service';
import { SharedService } from '../services/shared.service';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    private ngUnsubscribe = new Subject();

    error = null;
    loadingAnimation = false;
    resetMessage = {
        detail: ''
    };
    animationReset = false;

    cookieData = {
        signature: '',
        userlevel: '',
        username: ''
    };

    statusLogin = {
        status: '',
        detail: ''
    };

    constructor(
        public authService: AuthenticationLoopbackService,
        public router: Router,
        public loginService: LoginService,
        public sharedService: SharedService
    ) {}

    ngOnInit() {
        const messageFormReset = Object.assign(this.statusLogin, this.sharedService.retrieve_object());
        if (messageFormReset.status === 'logout') {
            this.resetMessage.detail = messageFormReset.detail;
            this.animationReset = true;
            setTimeout(() => {
                this.animationReset = false;
            }, 5000);
        }
    }

    onSubmit(data) {
        this.loadingAnimation = true;
        this.loginService.login(data.value).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            res => {
                if (res) {
                    const userData = Object.assign(this.cookieData, res[0]);
                    this.authService.setCookieUserData(userData);
                    const cookie = this.authService.getUserFull();
                    console.log(cookie);

                    if (cookie) {
                        this.router.navigateByUrl('/signage/dashboard');
                    }
                } else {
                    this.error = 'Username atau Password yang Anda Masukan Salah';
                    setTimeout(() => {
                        this.error = null;
                    }, 6000);
                    this.loadingAnimation = false;
                }
            }
        );
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
