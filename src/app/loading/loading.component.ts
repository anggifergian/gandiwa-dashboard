import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.css']
})
export class LoadingComponent {
    @HostBinding('style.position') get position() {
        return 'fixed';
    }
    @HostBinding('style.top') get top() {
        return '50%';
    }
    @HostBinding('style.left') get left() {
        return '50%';
    }
    @HostBinding('style.width') get width() {
        return '100%';
    }
    @HostBinding('style.height') get height() {
        return '100%';
    }
    @HostBinding('style.transform') get transform() {
        return 'translate(-50%, -50%)';
    }
    @HostBinding('style.background') get background() {
        return 'rgba(255, 255, 255, 0.6)';
    }
    @HostBinding('style.z-index') get zindex() {
        return '5000';
    }
}
