import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatInput } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppComponent } from './app.component';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { ChartModule } from 'primeng/chart';
import { ChartsModule } from 'ng2-charts';

import { AuthenticationLoopbackService } from './services/authentication-loopback.service';
import { LoginService } from './services/login.service';
import { SharedService } from './services/shared.service';
import { BannerService } from './signage/banner/banner.service';
import { DashboardService } from './signage/dashboard/dashboard.service';
import { StatisticService } from './signage/statistic/statistic.service';
import { VisitorService } from './signage/visitor/visitor.service';
import { AdminService } from './signage/admin/admin.service';

import { RemoveExtensionsPipe } from './pipes/remove-extensions.pipe';

import { LoginComponent } from './login/login.component';
import { LoadingComponent } from './loading/loading.component';
import { SpinnerComponent } from './signage/dashboard/spinner.component';
import { SignageComponent } from './signage/signage.component';
import { DashboardComponent } from './signage/dashboard/dashboard.component';
import { StatisticComponent } from './signage/statistic/statistic.component';
import { BannerComponent } from './signage/banner/banner.component';
import { AdminComponent } from './signage/admin/admin.component';
import { VisitorComponent } from './signage/visitor/visitor.component';
import { VisitorViewComponent } from './signage/visitor/visitor-view.component';
import { LocationComponent } from './signage/dashboard/location/location.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavbarLinkComponent } from './components/navbar/navbar-link.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    LoginComponent,
    SignageComponent,
    DashboardComponent,
    StatisticComponent,
    BannerComponent,
    AdminComponent,
    VisitorComponent,
    SpinnerComponent,
    VisitorViewComponent,
    RemoveExtensionsPipe,
    LocationComponent,
    NavbarComponent,
    NavbarLinkComponent
  ],
  imports: [
    TableModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatBadgeModule,
    MatToolbarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatTableModule,
    MatDatepickerModule,
    BsDatepickerModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true }),
    NgSelectModule,
    DropdownModule,
    BsDropdownModule,
    CalendarModule,
    ChartModule,
    ChartsModule
  ],
  providers: [
    LoginService,
    AuthenticationLoopbackService,
    CookieService,
    SharedService,
    BannerService,
    DashboardService,
    StatisticService,
    VisitorService,
    AdminService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
