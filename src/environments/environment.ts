export const environment = {
  production: false,

  apiPpdbLocal: 'http://localhost/restapippdb/',
  apiMain: 'https://gandiwa.funcafe.web.id/portal/',
  apiSignage: 'https://gandiwa.funcafe.web.id/portal/signage/',
  apiVisitor: 'https://gandiwa.funcafe.web.id/portal/visitor/',
  apiLoginGandiwa: 'https://gandiwa.funcafe.web.id/portal/login/'
};
