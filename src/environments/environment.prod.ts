export const environment = {
  production: true,

  apiPpdbLocal: 'http://localhost/restapippdb/',
  apiMain: 'https://gandiwa.funcafe.web.id/portal/',
  apiSignage: 'https://gandiwa.funcafe.web.id/portal/signage/',
  apiVisitor: 'https://gandiwa.funcafe.web.id/portal/visitor/',
  apiLoginGandiwa: 'https://gandiwa.funcafe.web.id/portal/login/'
};
